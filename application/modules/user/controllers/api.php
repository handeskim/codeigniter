<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller {
	function __construct(){
		parent::__construct();

		$this->r = array('status'=>200,'message'=>'error');
	}

	public function index_get(){
		$users = $this->mongo_db->get('user');
		$userResponse = [];
		foreach ($users as $user) {
			$userResponse[] = [
				'_id' => (string)$user['_id'],
				'name' => $user['name'],
				'birth_day' => $user['birth_day'],
				'sex' => $user['sex'],
				'balance' => $user['balance'],
				'created_at' => $user['created_at'],
			];
		}

		$this->r['message'] = 'success';
		$this->r['data'] = $userResponse;
		$this->response($this->r);
	}

	public function index_post(){
		$this->response($this->r);
	}

	public function index_put(){

		$this->response($this->r);
	}

	public function index_del(){
		$this->response($this->r);
	}
}

?>
