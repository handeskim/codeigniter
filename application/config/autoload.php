<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('mongo_db','session','parser', 'encrypt');
$autoload['helper'] = array('url_helper');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array();
